CREATE TABLE `draws` (
  `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `date` date NOT NULL
);

CREATE TABLE `draw_numbers` (
  `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `draw_id` int(10) NOT NULL,
  `numb` tinyint(3) NOT NULL
);