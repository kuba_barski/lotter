<?php

namespace App;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\DB;

class DrawCommand extends Command {
    private $collection = [];
    private $combinations = [];

    public function configure()
    {
        $this->setName('draw')
             ->setDescription('Fetch number collection and prepare next, most probable combination(s)')
             ->addOption('quantity', 't', InputOption::VALUE_OPTIONAL, 'Desired number of combinations', 2)
             ->addOption('collection', 'c', InputOption::VALUE_OPTIONAL, 'Size of rarest number collection to choose from', 10)
             ->addOption('reverse', 'r', InputOption::VALUE_NONE, 'Reverse order and use most common numbers')
             ->addOption('eliminate', 'e', InputOption::VALUE_NONE, 'Eliminate numbers from two last draws from collection');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        // Save options to short name variables
        $qty = $input->getOption('quantity');
        $col = $input->getOption('collection');
        $order = $input->getOption('reverse') ? 'DESC' : 'ASC';

        // Validate options
        if(!$this->validateInput($output, $qty, $col))
            return;

        // Fetch number collection from database
        $this->fetchCollection($order);

        // Remove numbers from last two draws if desired
        if($input->getOption('eliminate')) {
            $this->eliminateLastTwo();
        }

        // Narrow number collection to desired size
        $this->narrowCollection($col);

        // Prepare combinations
        $this->prapareCombinations($qty);

        // print_r($this->combinations);
        // print_r(sizeof($this->combinations));
        // return;

        $this->printCombinations($output);
    }

    private function validateInput(OutputInterface $output, $qty, $col)
    {
        if(!is_numeric($qty) || $qty < 1) {
            $output->writeln('<error>ERROR: Option quantity must be an integer and grather than 0</error>');
            return false;
        }

        if(!is_numeric($col) || $col < 6 || $col > 37) {
            $output->writeln('<error>ERROR: Option collection must be be an integer from 6 to 37</error>');
            return false;
        }

        return true;
    }

    private function fetchCollection($order)
    {
        // Get PDO
        $pdo = DB::getPdo();

        // Fetch number collection
        $this->collection = $pdo->query(
            'SELECT numb, count(*) as hits FROM draw_numbers GROUP BY numb ORDER BY hits '. $order
        )->fetchAll();
    }

    private function eliminateLastTwo()
    {
        // Get PDO
        $pdo = DB::getPdo();

        // Fetch numbers from last two draws
        $results = $pdo->query(
            'SELECT numb FROM `draw_numbers` JOIN draws ON draw_numbers.draw_id = draws.id WHERE draws.`date` > (SELECT `date` FROM `draws` ORDER BY `date` DESC LIMIT 2,1)'
        );

        // Eliminate obtained numbers from collection
        while($row = $results->fetch()) {
            foreach($this->collection as $key => $item) {
                if($item['numb'] == $row['numb']) {
                    unset($this->collection[$key]);
                    break;
                }
            }
        }
    }

    private function narrowCollection($col)
    {
        $this->collection = array_slice($this->collection, 0, $col);
    }

    private function prapareCombinations($qty)
    {
        for($i=0; $i<$qty; $i++) {
            // Randomize number collection
            shuffle($this->collection);

            // Save combination
            $this->combinations[] = array_slice($this->collection, 0, 6);
        }
    }

    private function printCombinations(OutputInterface $output)
    {
        $output->writeln('<info>Calculated combinations:</info>');

        // Echo out combinations
        foreach ($this->combinations as $combination) {
            // Sort combination
            usort($combination, function($a, $b) {
                return $a['numb'] <=> $b['numb'];
            });

            // Prepare combination string and calculete the sum
            $str = null;
            $sum = 0;

            foreach ($combination as $item) {
                $str .= $item['numb'].' ';
                $sum += $item['numb'];
            }

            $output->writeln("{$str}  sum({$sum})");
        }

        $output->writeln('');
    }
}
