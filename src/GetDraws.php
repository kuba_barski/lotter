<?php
namespace App;

class GetDraws {
    const BULK_URL = "http://www.mbnet.com.pl/dl.txt";

    const KEY_ID = 'id';
    const KEY_DATE = 'date';
    const KEY_NUMBERS = 'numbers';

    public function get($desc = false)
    {
        $parsed = [];
        $result = explode(PHP_EOL, $this->getResultByCurl());

        foreach($result as $row) {
            $splittedRow = explode(' ', $row);
            $parsedRow = [];

            if (empty($splittedRow[1]) || empty($splittedRow[2])) {
                continue;
            }

            $parsedRow[static::KEY_ID] = (int)str_replace('.', '', $splittedRow[0]);
            $parsedRow[static::KEY_DATE] = date('Y-m-d', strtotime($splittedRow[1]));
            $parsedRow[static::KEY_NUMBERS] = explode(',', $splittedRow[2]);

            $parsed[] = $parsedRow;
        }

        return $desc ? array_reverse($parsed) : $parsed;
    }

    private function getResultByCurl()
    {
      $curl = curl_init();

      curl_setopt($curl, CURLOPT_URL, static::BULK_URL);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

      $result = curl_exec($curl);

      curl_close($curl);

      return $result;
    }
}
