<?php

namespace App;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\DB;

class AvgSumCommand extends Command {
    public function configure()
    {
        $this->setName('avgsum')
             ->setDescription('Display average sum from all combinations');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        // Get PDO
        $pdo = DB::getPdo();

        // Fetch collection of numbers
        $average = $pdo->query(
            'SELECT sum(numb) / count(DISTINCT draw_id) as `avg` FROM draw_numbers'
        )->fetch();

        $output->writeln('<info>Average sum:</info>');
        $output->writeln($average['avg']);
        $output->writeln('');
    }
}
