<?php

namespace App;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\DB;
use App\GetDraws;
use \PDO;

class PopulateDatabase extends Command {
    public function configure()
    {
        $this->setName('populatedb')
             ->setDescription('Populate database with parsed draw archive.');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $pushCount = (new SaveResults)->save((new GetDraws)->get());
        $output->writeln('Database populated! ' . $pushCount . ' draw(s) saved.');
    }
}
