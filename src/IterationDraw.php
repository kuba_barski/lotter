<?php

namespace App;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class IterationDraw extends Command {
    const LAST_DRAWS_ELIMINATION = 2;
    const NUMBERS_TO_CHOOSE_FROM = 20;

    public function configure()
    {
        $this->setName('iterdraw')
            ->setDescription('Draw numbers by iteration elimination.')
            ->addOption('eliminate', 'e', InputOption::VALUE_OPTIONAL, 'Eliminate numbers from last X draws.', 2)
            ->addOption('collection', 'c', InputOption::VALUE_OPTIONAL, 'Size of the collection to choose from.', 15);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $pdo = DB::getPdo();

        $eliminate = $input->getOption('eliminate');
        $collection = $input->getOption('collection');

        // params validation?!

        $stmt = $pdo->query('
            SELECT n.numb, count(*) as hits
            FROM draw_numbers n
            WHERE n.numb NOT IN (
                SELECT n.numb
                FROM draw_numbers n
                WHERE n.draw_id IN (
                    SELECT d.id
                    FROM draws d
                    ORDER BY date
                    DESC LIMIT '. $eliminate .'
                )
            )
            GROUP BY n.numb
            ORDER BY hits DESC'
        );

        $results = $stmt->fetchAll();

        $offset = floor(count($results) / 2) - floor($collection / 2);
        $numbers = array_slice($results, $offset, $collection);

        // if numbers !== collection throw exception

        $combination = [];

        do {
            $drawed = $numbers[rand(1, $collection - 1)]['numb'];

            if (in_array($drawed, $combination)) {
                continue;
            }

            $combination[] = $drawed;
        } while (count($combination) < 6);

        sort($combination);

        $output->writeln('<info>Calculated combination:</info>');
        $output->writeln(implode(', ', $combination));
    }
}
