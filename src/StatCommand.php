<?php

namespace App;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\DB;

class StatCommand extends Command {
    public function configure()
    {
        $this->setName('stat')
             ->setDescription('Display number statistics');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        // Get PDO
        $pdo = DB::getPdo();

        // Fetch collection of numbers
        $numbers = $pdo->query(
            'SELECT numb, count(*) as hits FROM draw_numbers GROUP BY numb ORDER BY hits'
        )->fetchAll();

        $output->writeln('<info>Numbers pop out count:</info>');

        // Echo stats
        foreach ($numbers as $number) {
            // Conditional space adding for better formatting
            if($number['numb']<10)
                $number['numb'] .= ' ';

            $output->writeln("{$number['numb']} ({$number['hits']})");
        }

        $output->writeln('');
    }
}
