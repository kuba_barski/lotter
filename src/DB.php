<?php

namespace App;
use \PDO;

class DB {
    /** @var PDO */
    private static $pdo;

    private function __construct() {}
    private function __clone() {}

    /**
     * @return PDO
     */
    public static function getPdo()
    {
        if(self::$pdo === null) {
            self::$pdo = new PDO(DB_DRIVER . ':' . DB_FILE, '', '', [
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_NAMED
            ]);
        }

        return self::$pdo;
    }
}
