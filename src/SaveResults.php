<?php
namespace App;

use App\DB;
use App\GetDraws;

class SaveResults {
    public function save(array $draws)
    {
        $pdo = DB::getPdo();
        $pushCount = 0;

        $stmtDraw = $pdo->prepare(
          'INSERT INTO draws (`id`, `date`) VALUES (:id, :date)'
        );

        $stmtNumb = $pdo->prepare(
          'INSERT INTO draw_numbers (draw_id, numb) VALUES (:id, :numb1),
          (:id, :numb2), (:id, :numb3), (:id, :numb4), (:id, :numb5),
          (:id, :numb6)'
        );

        foreach($draws as $draw) {
            $stmtDraw->bindParam(':id', $draw[GetDraws::KEY_ID], \PDO::PARAM_INT);
            $stmtDraw->bindParam(':date', $draw[GetDraws::KEY_DATE], \PDO::PARAM_STR);
            $stmtDraw->execute();

            $stmtNumb->bindParam(':id', $draw[GetDraws::KEY_ID], \PDO::PARAM_INT);
            $stmtNumb->bindParam(':numb1', $draw[GetDraws::KEY_NUMBERS][0], \PDO::PARAM_INT);
            $stmtNumb->bindParam(':numb2', $draw[GetDraws::KEY_NUMBERS][1], \PDO::PARAM_INT);
            $stmtNumb->bindParam(':numb3', $draw[GetDraws::KEY_NUMBERS][2], \PDO::PARAM_INT);
            $stmtNumb->bindParam(':numb4', $draw[GetDraws::KEY_NUMBERS][3], \PDO::PARAM_INT);
            $stmtNumb->bindParam(':numb5', $draw[GetDraws::KEY_NUMBERS][4], \PDO::PARAM_INT);
            $stmtNumb->bindParam(':numb6', $draw[GetDraws::KEY_NUMBERS][5], \PDO::PARAM_INT);
            $stmtNumb->execute();

            $pushCount++;
        }

        $stmtDraw = null;
        $stmtNumb = null;

        return $pushCount;
    }
}
