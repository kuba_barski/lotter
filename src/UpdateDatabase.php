<?php

namespace App;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\DB;
use App\GetDraws;
use App\SaveResults;
use \PDO;

class UpdateDatabase extends Command {
    public function configure()
    {
        $this->setName('updatedb')
             ->setDescription('Update database with newest draws.');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $push = [];
        $lastDateDB = $this->getLastDrawDateFromDB();
        $draws = (new GetDraws)->get(true);


        foreach ($draws as $draw) {
            if ($draw[GetDraws::KEY_DATE] <= $lastDateDB) {
                break;
            }

            $push[] = $draw;
        }

        $pushCount = (new SaveResults)->save($push);
        $output->writeln('Database updated with ' . $pushCount . ' draw(s).');
    }

    private function saveResultsToDB(array $result)
    {
      $pdo = DB::getPdo();


      $stmtDraw = $pdo->prepare(
        'INSERT INTO draws (`id`, `date`) VALUES (:id, :date)'
      );

      $stmtNumb = $pdo->prepare(
        'INSERT INTO draw_numbers (draw_id, numb) VALUES (:id, :numb1),
        (:id, :numb2), (:id, :numb3), (:id, :numb4), (:id, :numb5),
        (:id, :numb6)'
      );

      foreach($result as $draw) {
          $stmtDraw->bindParam(':id', $draw[static::KEY_ID], PDO::PARAM_INT);
          $stmtDraw->bindParam(':date', $draw[static::KEY_DATE], PDO::PARAM_STR);
          $stmtDraw->execute();

          $stmtNumb->bindParam(':id', $draw[static::KEY_ID], PDO::PARAM_INT);
          $stmtNumb->bindParam(':numb1', $draw[static::KEY_NUMBERS][0], PDO::PARAM_INT);
          $stmtNumb->bindParam(':numb2', $draw[static::KEY_NUMBERS][1], PDO::PARAM_INT);
          $stmtNumb->bindParam(':numb3', $draw[static::KEY_NUMBERS][2], PDO::PARAM_INT);
          $stmtNumb->bindParam(':numb4', $draw[static::KEY_NUMBERS][3], PDO::PARAM_INT);
          $stmtNumb->bindParam(':numb5', $draw[static::KEY_NUMBERS][4], PDO::PARAM_INT);
          $stmtNumb->bindParam(':numb6', $draw[static::KEY_NUMBERS][5], PDO::PARAM_INT);
          $stmtNumb->execute();
      }

      $stmtDraw = null;
      $stmtNumb = null;
    }

    private function getLastDrawDateFromDB()
    {
        $result = DB::getPdo()
            ->query('SELECT `date` FROM draws ORDER BY `date` DESC LIMIT 1')
            ->fetch();

        return $result['date'];
    }
}
