# Lotter 0.1

## Description

Simple console app based on Symfony Console Component. It calculates lottery combinations
based on database archive of previous draws. For more info use a -h flag.

## Instalation

```
# Clone project
git clone https://gitlab.com/kuba_barski/lotter.git

# Install dependencies
cd lotter
composer install

```

Using /database/dump.sql import database structure and data. Create a user and update /config/config.php file accordingly.

## Usage

Example usage:
Calculate 3 combinations from a collection of 15 rarest numbers, after eliminating numbers that showed up in two last draws:

```
./lotter draw -e -c 15 -t 3
```